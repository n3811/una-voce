// [SECTION] Dependencies and Modules
const User = require('../models/User');
const Product = require('../models/Post');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// [SECTION] Functionalities [Create]
	// Create a new user
	module.exports.registerUser = (reqBody) => {
		let fName = reqBody.firstName;
		let lName = reqBody.lastName;
		let mName = reqBody.middleName;
		let email = reqBody.email;
		let passW = reqBody.password;
		let mobil = reqBody.mobileNo;
		let address = reqBody.address
		let newUser = new User ({
			firstName: fName,
			lastName: lName,
			middleName: mName,
			email: email,
			password: bcrypt.hashSync(passW, 10),
			mobileNo: mobil,
			address: address
		});
		return User.findOne({email:email}).then(found => {
			if (found) {
				return false
			} else {
				return newUser.save().then((user, err) => {
					if (user) {
						return user
					} else {
						return false
					}
				});
			}
		});

	};

	// check email
	module.exports.checkEmailExistence = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return 'Email Already Exists. Please use another email.';
			} else {
				return 'Email is still available.'
			}
		});
	};

	// Login user
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				// email does not exist
				return false
			} else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPassW, passW);
				if (isMatched === true) {
					let dataNiUser = result.toObject();
					return {access: auth.createAccessToken(dataNiUser)};					
				} else {
					return false
				};
			};
		});
	};

	// purchase product
	module.exports.purchase = async (data) => {
		let id = data.userId 
		let products = data.productId 
		let isUserUpdated = await User.findById(id).then(user => {
			user.orders.push({productId: products});
			return user.save().then((saved, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		});
		let isProductUpdated = await Product.findById(products).then(products => {
			products.orders.push({userId: id});
			return products.save().then((saved, err) => {
				if (err) {
					return false;
				} else {
					return true;
				};
			});
		});
		if (isUserUpdated && isProductUpdated) {
			return true 
		} else {
			return 'Purchase failed. Contact us for problems.'
		};
	};

// [SECTION] Functionalities [Retrieve]

	// Get all users
	module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
			return result;
		})
	};

	// Get profile
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};

// [SECTION] Funtionalities [Update]

	// Set User as Admin
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return 'Failed to set user as admin.';
			};
		});
	};

	// Set User as Non-Admin
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return true;
			} else {
				return 'Failed to set admin as user.';
			};
		});
	};

	// Update Password
	module.exports.changePassword = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return "Email does not exist.";
			} else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPassW, passW);
				if (isMatched) {
					let newPass = reqBody.newpassword;
					return User.findOneAndUpdate({email: uEmail}, {password: bcrypt.hashSync(newPass, 10)}).then((savedUser, err) => {
						if (err) {
							return false;
						} else {
							return 'Your password has been changed successfully. Please login again.';
						};
					});
				} else {
					return "Incorrect password. Check Credentials";
				};
			};
		});
	};
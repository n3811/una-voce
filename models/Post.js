const mongoose = require('mongoose');

const postBlueprint = new mongoose.Schema({
	title: {
		type: String,
		required: [true, 'Product Name required.']
	},
	description: {
		type: String,
		required: [true, 'Product Description required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Post", postBlueprint);